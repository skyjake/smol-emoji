# Smol Emoji

**Unicode symbols for the small internet**

![Sample glyphs: Duck, Eagle, Burrito](sample.png)

## What is this?

*Smol Emoji* is an OpenType/TrueType font that aims to provide a full set of black & white vector graphic Emojis and other Unicode pictographs for use on the "smol internet".

A key design objective is that the glyphs are minimal in detail, therefore yielding a font file that is minimal in size.

The black and white format has been chosen so the glyphs can be presented in any foreground/background color combination.

*Smol Emoji* is licensed under the SIL Open Font License v1.1.

## Background

The font was created for use in the [Lagrange Gemini browser][lgr] to provide additional glyphs that were missing from [Noto Emoji][ne] and [Noto Symbols][ns].

## Status

The initial version of the font was created in May 2021 and contained 95 face emoticons. The current (and first) objective is to cover all Emoji up to Unicode 13 while also relying on Noto Emoji for the older glyphs.

View the [Test Page](emoji-test.gmi) in your Gemini browser to see which glyphs are missing: [gemini://skyjake.fi/smol-emoji/emoji-test.gmi](gemini://skyjake.fi/smol-emoji/emoji-test.gmi)

Note that Lagrange v1.4 and earlier use Symbola so it will show complete Emoji coverage. Lagrange v1.5 and later do not include Symbola and instead of Smol Emoji, Noto Emoji, and a custom font of your choosing. When viewing the test page, check that you have no custom font configured.

## Guidelines

* "Minimal" does not mean that the depicted objects are unrecognizable.
* "Minimal" *does* mean that you should consider how to represent objects with the minimum number of points and curves. Filled shapes require less data, but a given shape may not be distinctive enough without contrasting elements.
* The target for a single glyph is ~100 vertices. A glyph should not exceed 200 vertices (these numbers include curve control points).

[lgr]: https://gmi.skyjake.fi/lagrange/
[ne]: https://github.com/googlefonts/noto-emoji
[ns]: https://www.google.com/get/noto/#sans-zsym